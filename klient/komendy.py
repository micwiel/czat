#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Klasa dozwolonych komend.
"""

slownik = {}
slownik['lista_uzytkownikow'] = 1
slownik['tekst'] = 2
slownik['nick'] = 3
slownik['rozlacz'] = 4
slownik['inicjacja'] = 5


class Komenda:
    def __init__(self, komenda):
        if not komenda in slownik:
            raise
        else:
            self.wartosc = slownik[komenda]