#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plik wykonywalny klienta.
"""

import socket
from protokol import Datagram
import pickle
from komendy import Komenda


class Polaczenie:
    def __init__(self, serwer, port):
        self.SERWER, self.PORT = serwer, int(port)
        self.polacz()
        self.ip, self.id, self.polaczenie = self.ustalenia()

    def polacz(self):
        """ Otwiera gniazdo i nawiązuje połączenie. """
        # !!! wybieranie protokołu
        # sekcja ipv4
        #self.gniazdo = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # sekcja ipv6
        self.gniazdo = socket.socket(socket.AF_INET6, socket.SOCK_STREAM, 0)
        self.gniazdo.connect((self.SERWER, self.PORT))
        print('Nawiązano połączenie.')

    def wyslij(self, datagram):
        """ Bezpośrednie wysłanie do użytku wewnętrznego. """
        # sprawdzamy czy nie wrzucono czegoś niedozwolonego
        if isinstance(datagram, Datagram):
            datagram = pickle.dumps(datagram)
            self.gniazdo.sendall(datagram)
        else:
            raise
        odpowiedz = self.gniazdo.recv(1024)
        return pickle.loads(odpowiedz)

    def ustalenia(self):
        """ Początkowe ustalenia. """
        # konstruujemy inicjujący datagram
        datagram = Datagram(0, 0, 0, Komenda('inicjacja'), 0)
        odpowiedz = self.wyslij(datagram)
        return odpowiedz

    def rozlacz(self):
        """ Poprawne rozłączenie. """
        datagram = Datagram(self.ip, self.id, self.polaczenie, Komenda('rozlacz'), None)
        odpowiedz = self.wyslij(datagram)
        self.gniazdo.close()
        print(odpowiedz)
        print('Rozłączono.')