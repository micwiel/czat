#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Protokół komunikacyjny.
"""

from komendy import Komenda


class Datagram:
    def __init__(self, ip, id_, polaczenie, komenda, dane=None):
        self.ip = ip
        self.id = id_
        self.polaczenie = polaczenie
        if not isinstance(komenda, Komenda):
            raise
        else:
            self.komenda = komenda
        self.dane = dane