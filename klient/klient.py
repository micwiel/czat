#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plik wykonywalny klienta.
"""

import socket
import komendy
from polaczenie import Polaczenie
from protokol import Datagram
from komendy import Komenda


def menu():
    """ Funkcja wyświetlająca menu. """
    print('Witaj użytkowniku SuperCzatu.')
    print('By wyświetlić pomoc wpisz /pomoc')


def pomoc():
    """ Funkcja wyświetlająca pomoc. """
    print('/serwer nazwa_serwera:port - łączy z wybranym serwerem')
    print('/rozłącz - rozłącza')
    print('/nick mójnick - ustawia nick')
    print('/mójnick - wyświetla nick')
    print('/lista - wyświetla listę podłączonych użytkowników')
    print('/id - wyświetla id połączenia')


class Klient:
    """ Zarządzanie połączeniem. """
    def __init__(self):
        self.nick = 'gość'

    def polacz(self, serwer, port):
        self.polaczenie = Polaczenie(serwer, port)
        if self.nick != 'gość':
            self.ustaw_nick(self.nick)

    def rozlacz(self):
        """ Poprawne rozłączenie. """
        # próbujemy rozłączyć (jeśli połączenie istniało)
        try:
            self.polaczenie.rozlacz()
            del(self.polaczenie)
        except NameError:
            print('Nie nawiązano żadnego połączenia.')

    def datagram(self, komenda, dane):
        """ Zwraca gotowy datagram. """
        return Datagram(self.polaczenie.ip, self.polaczenie.id, self.polaczenie.polaczenie, komenda, dane)

    def tekst(self, tekst):
        """ Wysyła tekst na czat. """
        datagram = self.datagram(Komenda('tekst'), tekst)
        return self.polaczenie.wyslij(datagram)

    def ustaw_nick(self, nick):
        """ Ustawia nick na serwerze. """
        self.nick = nick
        try:
            datagram = self.datagram(Komenda('nick'), nick)
            return self.polaczenie.wyslij(datagram)
        except:
            pass

    def lista_uzytkownikow(self):
        """ Zwraca listę podłączonych użytkowników. """
        datagram = self.datagram(Komenda('lista_uzytkownikow'), None)
        return self.polaczenie.wyslij(datagram)


def serwer(klient, argument):
    """ Nawiązanie połączenia z serwerem. """
    # próbujemy ustawić serwer
    try:
        serwer = argument.split(':')[0]
    except IndexError:
        serwer = 'localhost'

    # próbujemy ustawić port
    try:
        port = argument.split(':')[1]
    except (IndexError, NameError):
        port = 9999

    klient.polacz(serwer, port)


def koniec(klient):
    """ Poprawne zakończenie programu. """
    try:
        klient.rozlacz()
    except:
        pass
    exit()


if __name__ == "__main__":
    menu()
    klient = Klient()
    while True:
        print('>', end=' ')  # prompt
        polecenie = input().strip()

        try:
            if polecenie[0] == '/':  # jeśli linia zaczyna się od ukośnika
                operacja = polecenie.split(' ')[0]
                try:
                    argument = polecenie.split(' ')[1]
                except IndexError:
                    argument = None

                try:
                    if klient.polaczenie:  # jeśli nawiązano połaczenie
                        if operacja == '/rozłącz':
                            klient.rozlacz()
                            continue

                        elif operacja == '/lista':
                            print(klient.lista_uzytkownikow())
                            continue

                except:  # lokalne polecenia
                    pass

                if operacja == '/pomoc':
                    pomoc()

                elif operacja == '/serwer':
                    if argument is None:
                        argument = 'localhost:9999'  # domyślnie
                    try:
                        serwer(klient, argument)
                    except ConnectionRefusedError:
                        print('Problem z serwerem.')

                elif operacja == '/nick':
                    if argument is None:
                        argument = 'gość'  # domyślnie

                    klient.ustaw_nick(argument)

                elif operacja == '/mójnick':
                    print(klient.nick)

                elif operacja == '/id':
                    print(klient.polaczenie.id)

                else:  # każda inna operacja to wyjście
                    koniec(klient)

            else:  # brak początkowego ukośnika
                # próbujemy wysłać tekst
                try:
                    print(klient.tekst(polecenie))
                except AttributeError:
                    print('Nie nawiązano połączenia.')

        except IndexError:  # wpisywanie pustych enterów
            pass