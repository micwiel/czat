#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from random import random


class Uzytkownik:
    """ Klasa użytkownika na serwerze. """
    def __init__(self, ip, polaczenie):
        self.id = int(random() * 10 ** 7)
        self.ip = ip
        self.polaczenie = polaczenie
        self.nick = 'gość'

    def ustaw_nick(self, nick):
        self.nick = nick

    def __repr__(self):
        return str(self.id) + " " + str(self.ip) + " " + str(self.nick)