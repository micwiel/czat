#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Wiadomosc:
    def __init__(self, od_kogo, data, tresc):
        self.od_kogo = str(od_kogo.nick)
        self.data = str(data)
        self.tresc = tresc

    def __repr__(self):
        return self.data + " <" + self.od_kogo + "> " + self.tresc