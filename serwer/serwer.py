#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Plik wykonywalny serwera.
"""

import socketserver
from datetime import datetime
import socket
import multiprocessing as m
import komendy
import pickle
from uzytkownik import Uzytkownik
from wiadomosc import Wiadomosc


lista_uzytkownikow = []
lista_watkow = []


def znajdz_uzytkownika(polaczenie):
    for i in lista_uzytkownikow:
        if i.polaczenie == polaczenie:
            return i
    return None


class ObslugaZadan(socketserver.BaseRequestHandler):
    """ Klasa obsługująca żądania do serwera. """
    def log(self, tresc):
        """ Produkuje logi serwera. """
        log = str(datetime.now()) + "  " + str(tresc)
        return log

    def zapytanie(self):
        """ Wyświetla zapytania do serwera. """
        return self.log("> " + self.client_address[0] + "  " + str(self.dane))

    def odpowiedz(self, tresc):
        """ Wyświetla odpowiedzi serwera. """
        return self.log("< " + self.client_address[0] + "  " + str(tresc))

    def rozeslij(self, wiadomosc):
#        gniazdo = self.request
#        try:
#            gniazdo.sendall(pickle.dumps(tekst))  # odeślij
#            print(self.odpowiedz(tekst))  # produkuj logi
#        except (EOFError, OSError):
#            print(self.log('puste dane, pomijam'))
#            # odłączamy gniazdo by obsługiwać kolejne żądania
#            gniazdo.detach()
        return wiadomosc

    def operacja(self, komenda, argument):
        """ Obsługa operacji. """
        # dołączył nowy użytkownik
        if komenda.wartosc == komendy.Komenda('inicjacja').wartosc:
            uzytkownik = Uzytkownik(self.client_address[0], self.client_address[1])
            lista_uzytkownikow.append(uzytkownik)
            if argument is not None:
                uzytkownik.ustaw_nick(argument)

            return uzytkownik.ip, uzytkownik.id, uzytkownik.polaczenie

        # lista użytkowników
        elif komenda.wartosc == komendy.Komenda('lista_uzytkownikow').wartosc:
            return str(lista_uzytkownikow)

        # grzeczne rozłączenie
        elif komenda.wartosc == komendy.Komenda('rozlacz').wartosc:
            uzytkownik = znajdz_uzytkownika(self.client_address[1])
            lista_uzytkownikow.remove(uzytkownik)
            return str(uzytkownik)

        # zmiana nicka
        elif komenda.wartosc == komendy.Komenda('nick').wartosc:
            uzytkownik = znajdz_uzytkownika(self.client_address[1])
            uzytkownik.ustaw_nick(argument)
            return argument

        # zwyczajna wiadomość tekstowa
        elif komenda.wartosc == komendy.Komenda('tekst').wartosc:
            od_kogo = znajdz_uzytkownika(self.client_address[1])
            data = datetime.now().strftime('%y-%m-%d %H:%M')
            wiadomosc = Wiadomosc(od_kogo, data, argument)
            return self.rozeslij(wiadomosc)

        # standardowa odpowiedź
        else:
            return "Twoje ip to " + self.client_address[0]

    def handle(self):
        """ Obsługa pojedyńczego żądania. """
        try:
            while True:  # działaj w nieskończoność
                gniazdo = self.request
                self.dane = gniazdo.recv(1024).strip()  # otrzymane dane
                print(self.zapytanie())  # produkuj logi
                try:
                    dane = pickle.loads(self.dane)
                    komenda = dane.komenda  # wyznacz żądanie
                    argument = dane.dane  # wyznacz argument
                    odpowiedz = self.operacja(komenda, argument)  # zwróć wynik
                    gniazdo.sendall(pickle.dumps(odpowiedz))  # odeślij
                    print(self.odpowiedz(odpowiedz))  # produkuj logi
                except (EOFError, OSError):
                    print(self.log('puste dane, pomijam'))
                    # odłączamy gniazdo by obsługiwać kolejne żądania
                    gniazdo.detach()
        except:
#            print(self.log('zerwano połączenie'))
            raise


class SerwerBazowy(socketserver.ThreadingTCPServer):
    """ Klasa pomagająca obsługiwać wątki. """
    def process_request(self, request, client_address):
        """Start a new thread to process the request."""
        import threading
        t = threading.Thread(target = self.process_request_thread,
                             args = (request, client_address))
        t.daemon = self.daemon_threads
        t.start()
        lista_watkow.append(t)


class SerwerIPv4(SerwerBazowy):
    """ Serwer z obsługą wątków nasłuchujący na ipv4. """
    pass


class SerwerIPv6(SerwerBazowy):
    """ Serwer z obsługą wątków nasłuchujący na ipv6. """
    address_family = socket.AF_INET6


class Serwer:
    """ Obsługa wszystkich podserwerów wraz z podziałem na procesy. """

    def serwer4(self):
        try:
            self.serwer4 = SerwerIPv4((self.host4, self.port), ObslugaZadan)
            self.serwer4.serve_forever()
        except:
            print('coś obecnie nasłuchuje na wybranym porcie')

    def serwer6(self):
        try:
            self.serwer6 = SerwerIPv6((self.host6, self.port), ObslugaZadan)
            self.serwer6.serve_forever()
        except:
            print('coś obecnie nasłuchuje na wybranym porcie')

    def __init__(self, host4='localhost', host6='localhost', port='9999'):
        self.host4 = host4
        self.host6 = host6
        self.port = port

        proces1 = m.Process(target=self.serwer4)
        proces1.start()
        proces2 = m.Process(target=self.serwer6)
        proces2.start()


if __name__ == "__main__":
    HOST4, HOST6, PORT = "0.0.0.0", "::1", 9999  # !!! nie działa :: (dziwne)

    # odpalenie serwera
    serwer = Serwer(HOST4, HOST6, PORT)